const Gamedig = require('gamedig');
const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

while (true) {
    Gamedig.query({
        type: 'arma3',
        host: '149.56.29.23'
    }).then((state) => {
        console.log('Current players: ' + state.players.length);
        console.log('Ping: ' + state.ping)
    }).catch((error) => {
        console.log("Server is offline");
    });

    await sleep(5);
}
